// Fill out your copyright notice in the Description page of Project Settings.


#include "ALS_Player_Controller.h"

#include "ALSVLearnDemo/CameraSystem/ALS_CameraManager.h"

void AALS_Player_Controller::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	AALS_CameraManager* CameraManager = Cast<AALS_CameraManager>(PlayerCameraManager);
	if (CameraManager)
	{
		CameraManager->OnPossess(InPawn);
	}
}
